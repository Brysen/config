" _   _                 _
"| \ | | ___  _____   _(_)_ __ ___
"|  \| |/ _ \/ _ \ \ / / | '_ ` _ \
"| |\  |  __/ (_) \ V /| | | | | | |
"|_| \_|\___|\___/ \_/ |_|_| |_| |_|

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Setup
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" {{{
set nocompatible
filetype plugin on
syntax on
set bg=light
"set termguicolors
"colorscheme NeoSolarized
"set runtimepath^=~/.config/nvim runtimepath+=~/.config/nvim/after
"let &packpath = &runtimepath
"}}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Options
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" {{{
set list
set listchars=tab:>-,trail:~
"set expandtab
set tabstop=4
set shiftwidth=4
set autoindent
set cursorline
set encoding=utf-8
" set laststatus=2
set ignorecase
set nohlsearch
set incsearch
set wildmode=longest,list,full " Enable autocompletion:
set path+=** " Recursive search into subfolders with find
" }}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Folding
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" {{{
set foldmethod=marker
nnoremap <Space> za
"}}}
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 User Interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" {{{
highlight ColorColumn ctermbg=red
set colorcolumn=80
set number relativenumber
" }}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" {{{
if ! filereadable(expand('~/.config/nvim/autoload/plug.vim'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ~/.config/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ~/.config/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif
call plug#begin('~/.config/nvim/plugged')

Plug 'junegunn/goyo.vim' " Centers text

Plug 'vimwiki/vimwiki' " Vimwiki for Markdown
    let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}

Plug 'terryma/vim-multiple-cursors' " what it sounds like

Plug 'tpope/vim-commentary' " commenting

Plug 'jpalardy/vim-slime' " Slime (ide features)
    let g:slime_target = "tmux"
    let g:slime_default_config = {"socket_name": get(split($TMUX, ","), 0), "target_pane": ":.2"}
    let g:slime_paste_file = "/tmp/slimepaste"

Plug 'junegunn/rainbow_parentheses.vim' " Rainbow brackets

Plug 'junegunn/limelight.vim' " Limelight
	let g:limelight_default_coefficient = 0.7
	let g:limelight_conceal_ctermfg = 240
	autocmd! User GoyoEnter Limelight
	autocmd! User GoyoLeave Limelight!

call plug#end()
" }}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Netrw
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" {{{
let g:netrw_banner=0
let g:netrw_browse_split=4
let g:netrw_altv=1
let g:netrw_liststyle=3
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',/(^\|\s\s\)\zs\.\S\+'
" }}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Auto Commands
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" {{{
autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
autocmd BufRead,BufNewFile *.tex set filetype=tex
" Deletes trailing whitespace
autocmd BufWritePre * %s/\s\+$//e
" Compiles Suckless uitilities when the config headers are updated
autocmd BufWritePost config.h,config.def.h !sudo make install
" Compiles dwmbocks when the block header is updated
autocmd BufWritePost blocks.h !sudo make install && killall dwmblocks ; setsid dwmblocks &
" Reloads X resources after editing
autocmd BufWritePost ~/.config/x11/Xresources !theme &
" Restarts sxhkd whenever the config is updated
autocmd BufWritePost ~/.config/sxhkd/sxhkdrc !pkill -USR1 -x sxhkd
" Restarts sxhkd whenever the config is updated
autocmd BufWritePost ~/.local/bin/status/* !killall dwmblocks ; setsid dwmblocks &
" Restarts picom whenever the config is updated
autocmd BufWritePost ~/.config/picom.conf !pkill picom; setsid picom &
 " Reruns the shortcut script when the configs are updated
autocmd BufWritePost ~/.config/files !shortcuts &
autocmd BufWritePost ~/.config/directories !shortcuts &
" Changes filetypes
autocmd BufRead,BufNewFile *.ms set filetype=groff
" Runs a script that cleans out tex build files whenever I close out of a .tex file.
autocmd VimLeave *.tex !texclear %
" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" }}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" {{{
    function! FixLastSpellingError()
    		normal! mm[s1z=`m
    endfunction
    function! SourceRange() range
      let tmpsofile = tempname()
      call writefile(getline(a:firstline, a:lastline), l:tmpsofile)
      execute "source " . l:tmpsofile
      call delete(l:tmpsofile)
    endfunction
" }}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Splits
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" {{{
set splitbelow splitright
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
nnoremap <C-w>n :vsp new
" }}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Clipboard
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" {{{
vnoremap <C-c> "+y
map <C-p> "+p
set clipboard+=unnamedplus
" }}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Snippets
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Snippets {{{
" Markdown
autocmd FileType markdown inoremap ;1	<Esc>0YpVr=A<Enter><Enter>
autocmd FileType markdown inoremap ;2	<Esc>0YpVr-A<Enter><Enter>
autocmd FileType markdown inoremap ;3	###<space><Enter><Enter><++><Esc>2kA
autocmd FileType markdown inoremap ;4	####<space><Enter><Enter><++><Esc>2kA
autocmd FileType markdown inoremap ;5	#####<space><Enter><Enter><++><Esc>2kA
autocmd FileType markdown inoremap ;6	######<space><Enter><Enter><++><Esc>2kA
autocmd FileType markdown inoremap ;B	******<space><++><Esc>F*2hi
autocmd FileType markdown inoremap ;b	****<space><++><Esc>F*hi
autocmd FileType markdown inoremap ;t	**<space><++><Esc>F*i
autocmd FileType markdown inoremap ;s	~~<space><++><Esc>F~i
autocmd FileType markdown inoremap ;c	``<space><++><Esc>F`i
autocmd FileType markdown inoremap ;C	```<space><Enter><++><Enter>```<Enter><++><Esc>3kA
autocmd FileType markdown inoremap ;p	![](<++>)<space><++><Esc>F[a
autocmd FileType markdown inoremap ;u	[](<++>)<space><++><Esc>F[a
autocmd FileType markdown inoremap ;q	><space>"**" <++><Esc>F*i
" Shell scripting
autocmd FileType sh inoremap ;if		if;<CR>then<CR><++><CR>fi<Esc>?;<CR>i<space>
autocmd FileType sh inoremap ;case		case<CR>esac<up><space>in<CR><++>) <++><CR>;;<Esc>ykjp?case<CR>ea<space>
autocmd FileType sh inoremap ;for 		for<space>in<space><++>;<CR>do<CR><++><CR>done<Esc>?for<CR>ea<space>
autocmd FileType sh inoremap ;while 	while;<CR>do<CR><++><CR>done<Esc>?while<CR>ea<space>
autocmd FileType sh inoremap ;[-		[<space>-<space>"<++>" ]<++><Esc>F-a
autocmd FileType sh inoremap ;[!		[<space>!<space>-<space>"<++>" ]<++><Esc>F-a
autocmd FileType sh inoremap ;"=		[<space>"*"<space>=<space>"<++>"<space>]<++><Esc>F*cl
autocmd FileType sh inoremap ;"!		[<space>"*"<space>!=<space>"<++>"<space>]<++><Esc>F*cl
autocmd FileType sh inoremap ;v 		"$"<++><Esc>F$a
autocmd FileType sh inoremap ;sb 		$()<++><Esc>F(a
autocmd FileType sh inoremap ;func		()<space>{<CR><++><CR>}<Esc>?(<CR>i
autocmd FileType sh inoremap ;def		="<++>"<Esc>F=i
" Lisp
autocmd FileType lisp inoremap ((		(<space>)<Esc>F(a<space>
" Perl
autocmd FileType perl inoremap ;ar @=(<++>);<Esc>F@a
" }}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Misc Binds
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" {{{
nnoremap S :%s//g<left><left>
inoremap <up> <Nop>
inoremap <down> <Nop>
inoremap <left> <Nop>
inoremap <right> <Nop>
nnoremap <up> <Nop>
nnoremap <down> <Nop>
nnoremap <left> <Nop>
nnoremap <right> <Nop>
vnoremap <up> <Nop>
vnoremap <down> <Nop>
vnoremap <left> <Nop>
vnoremap <right> <Nop>
inoremap ;" ""<++><Esc>F"i
inoremap ;' ''<++><Esc>F'i
inoremap ;( ()<++><Esc>F(a
inoremap ;[ []<++><Esc>F[a
inoremap ;{ {}<++><Esc>F{a
inoremap ;< <><++><Esc>2F<a

nnoremap H 0
nnoremap L $

inoremap jj <Esc>
" }}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Leader
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"{{{
let mapleader =","
inoremap <leader><leader> <Esc>/<++><Enter>"_c4l
nnoremap <leader>sp :call FixLastSpellingError()<cr>
nnoremap <leader>p :!opout <c-r>%<CR><CR>
nmap <leader>' gcc
map <leader>rc :source $MYVIMRC<CR>
map <leader>l :Limelight!!<CR>
map <leader>c :w! \| !compiler <c-r>%<CR>
map <leader>o :setlocal spell! spelllang=en_us<CR>
map <leader>g :Goyo \| set linebreak<CR>
" }}}
