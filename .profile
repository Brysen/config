#!/bin/zsh

# load environmental variables
[ -f "$HOME/.config/env" ] && source "$HOME/.config/env"

# load shortcuts
shortcuts >/dev/null 2>&1

# Start graphical server on tty1 if not already running.
[ "$(tty)" = "/dev/tty1" ] && ! pgrep -x Xorg >/dev/null && exec startx
